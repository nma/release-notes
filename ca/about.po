# Catalan translation of the Debian Release Notes
#
# Miguel Gea Milvaques, 2006-2009.
# Jordà Polo, 2007, 2009.
# Guillem Jover <guillem@debian.org>, 2007, 2019.
# Héctor Orón Martínez, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: release-notes 10\n"
"POT-Creation-Date: 2019-06-15 19:17+0200\n"
"PO-Revision-Date: 2019-06-15 19:56+0200\n"
"Last-Translator: Guillem Jover <guillem@debian.org>\n"
"Language-Team: Catalan <debian-l10n-catalan@lists.debian.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Attribute 'lang' of: <chapter>
#: en/about.dbk:8
msgid "en"
msgstr "ca"

#. type: Content of: <chapter><title>
#: en/about.dbk:9
msgid "Introduction"
msgstr "Introducció"

#. type: Content of: <chapter><para>
#: en/about.dbk:11
msgid ""
"This document informs users of the &debian; distribution about major changes "
"in version &release; (codenamed &releasename;)."
msgstr ""
"Este document informa als usuaris de la distribució &debian; dels principals "
"canvis a la versió &release; (nom en clau &releasename;)."

#. type: Content of: <chapter><para>
#: en/about.dbk:15
msgid ""
"The release notes provide information on how to upgrade safely from release "
"&oldrelease; (codenamed &oldreleasename;) to the current release and inform "
"users of known potential issues they could encounter in that process."
msgstr ""
"Les notes de llançament ofereixen informació sobre com actualitzar de forma "
"segura des de la versió &oldrelease; (nom en clau &oldreleasename;) a la "
"versió actual, i informen als usuaris de problemes potencials coneguts que "
"es poden trobar durant eixe procés."

#. type: Content of: <chapter><para>
#: en/about.dbk:21
msgid ""
"You can get the most recent version of this document from <ulink url=\"&url-"
"release-notes;\"></ulink>.  If in doubt, check the date on the first page to "
"make sure you are reading a current version."
msgstr ""
"Podeu aconseguir la versió més recent d'este document a <ulink url=\"&url-"
"release-notes;\"></ulink>. Si teniu dubtes, comproveu la data del document "
"que hi ha a la primera pàgina i assegureu-vos que esteu llegint la darrera "
"versió."

#. type: Content of: <chapter><caution><para>
#: en/about.dbk:27
msgid ""
"Note that it is impossible to list every known issue and that therefore a "
"selection has been made based on a combination of the expected prevalence "
"and impact of issues."
msgstr ""
"Tingueu en compte que és impossible fer una llista amb tots els problemes "
"coneguts i per tant ha calgut fer una selecció basada en una combinació de "
"l'abast i l'impacte dels problemes."

#. type: Content of: <chapter><para>
#: en/about.dbk:33
msgid ""
"Please note that we only support and document upgrading from the previous "
"release of Debian (in this case, the upgrade from &oldreleasename;).  If you "
"need to upgrade from older releases, we suggest you read previous editions "
"of the release notes and upgrade to &oldreleasename; first."
msgstr ""
"Tingueu en compte també, que tan sols es suporta i documenta l'actualització "
"des de la versió anterior de Debian (en este cas, l'actualització des de "
"&oldrelease;). Si necessiteu actualitzar des de versions més antigues, us "
"suggerim que llegiu primer les edicions anteriors de les notes de llançament "
"i actualitzeu primer a &oldrelease;."

#. type: Content of: <chapter><section><title>
#: en/about.dbk:41
msgid "Reporting bugs on this document"
msgstr "Informar d'errors d'este document"

#. type: Content of: <chapter><section><para>
#: en/about.dbk:43
msgid ""
"We have attempted to test all the different upgrade steps described in this "
"document and to anticipate all the possible issues our users might encounter."
msgstr ""
"S'ha intentat fer comprovacions dels diversos passos del procés "
"d'actualització que es descriuen en este document, intentant també anticipar "
"possibles problemes que els nostres usuaris puguen trobar."

#. type: Content of: <chapter><section><para>
#: en/about.dbk:48
msgid ""
"Nevertheless, if you think you have found a bug (incorrect information or "
"information that is missing)  in this documentation, please file a bug in "
"the <ulink url=\"&url-bts;\">bug tracking system</ulink> against the "
"<systemitem role=\"package\">release-notes</systemitem> package. You might "
"first want to review the <ulink url=\"&url-bts-rn;\">existing bug reports</"
"ulink> in case the issue you've found has already been reported. Feel free "
"to add additional information to existing bug reports if you can contribute "
"content for this document."
msgstr ""
"No obstant això, si creieu que heu trobat un error en aquesta documentació "
"(informació incorrecta o manca d'informació), envieu un informe d'error al "
"<ulink url=\"&url-bts;\">sistema de seguiment d'errors</ulink>, al paquet "
"<systemitem role=\"package\">release-notes</systemitem>. Reviseu primer si "
"hi ha cap <ulink url=\"&url-bts-rn;\">informe d'error existent</ulink> en "
"cas de que ja s'haja informat del problema. No dubteu a afegir qualsevol "
"informació addicional als informes d'error ja existents si creieu que podeu "
"contribuir contingut a aquest document."

#. type: Content of: <chapter><section><para>
#: en/about.dbk:60
msgid ""
"We appreciate, and encourage, reports providing patches to the document's "
"sources. You will find more information describing how to obtain the sources "
"of this document in <xref linkend=\"sources\"/>."
msgstr ""
"Apreciem, i animem, a proporcionar informes amb fitxers de diferències de "
"les fonts del document. Trobareu més informació que descriu com obtenir les "
"fonts d'aquest document en <xref linkend=\"sources\"/>."

#. type: Content of: <chapter><section><title>
#: en/about.dbk:68
msgid "Contributing upgrade reports"
msgstr "Col·laborar amb informes d'actualització"

#. type: Content of: <chapter><section><para>
#: en/about.dbk:70
msgid ""
"We welcome any information from users related to upgrades from "
"&oldreleasename; to &releasename;.  If you are willing to share information "
"please file a bug in the <ulink url=\"&url-bts;\">bug tracking system</"
"ulink> against the <systemitem role=\"package\">upgrade-reports</systemitem> "
"package with your results.  We request that you compress any attachments "
"that are included (using <command>gzip</command>)."
msgstr ""
"Qualsevol informació dels usuaris relacionada amb actualitzacions des "
"d'&oldreleasename; a &releasename; és benvinguda. Si voleu compartir alguna "
"informació, envieu un informe d'error amb els vostres resultats al <ulink "
"url=\"&url-bts;\">sistema de seguiment d'errors</ulink>, al paquet "
"<systemitem role=\"package\">upgrade-reports</systemitem>. Us demanem que "
"comprimiu qualsevol adjunt que envieu (fent ús del <command>gzip</command>)."

#. type: Content of: <chapter><section><para>
#: en/about.dbk:79
msgid ""
"Please include the following information when submitting your upgrade report:"
msgstr ""
"Quan envieu l'informe d'actualització, assegureu-vos d'incloure la "
"informació següent:"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/about.dbk:86
msgid ""
"The status of your package database before and after the upgrade: "
"<systemitem role=\"package\">dpkg</systemitem>'s status database available "
"at <filename>/var/lib/dpkg/status</filename> and <systemitem role=\"package"
"\">apt</systemitem>'s package state information, available at <filename>/var/"
"lib/apt/extended_states</filename>.  You should have made a backup before "
"the upgrade as described at <xref linkend=\"data-backup\"/>, but you can "
"also find backups of <filename>/var/lib/dpkg/status</filename> in <filename>/"
"var/backups</filename>."
msgstr ""
"L'estat de la vostra base de dades de paquets abans de l'actualització: La "
"base de dades d'estat de <command>dpkg</command> que trobareu a <filename>/"
"var/lib/dpkg/status</filename> i la informació d'estat d' <systemitem role="
"\"package\">apt</systemitem> que trobareu a <filename>/var/lib/apt/"
"extended_states</filename>. Hauríeu d'haver fet un còpia de seguretat abans "
"d'actualitzar com es descriu a <xref linkend=\"data-backup\"/>, però també "
"podeu trobar còpies de seguretat a <filename>/var/lib/dpkg/status</filename> "
"en <filename>/var/backups</filename>."

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/about.dbk:99
msgid ""
"Session logs created using <command>script</command>, as described in <xref "
"linkend=\"record-session\"/>."
msgstr ""
"Els registres de sessió creats amb <command>script</command>, tal i com es "
"descriu a <xref linkend=\"record-session\"/> ."

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/about.dbk:105
msgid ""
"Your <systemitem role=\"package\">apt</systemitem> logs, available at "
"<filename>/var/log/apt/term.log</filename>, or your <command>aptitude</"
"command> logs, available at <filename>/var/log/aptitude</filename>."
msgstr ""
"Els registres d'<systemitem role=\"package\">apt</systemitem> els podeu "
"trobar a <filename>/var/log/apt/term.log</filename>, i els registres "
"d'<command>aptitude</command> els trobareu a <filename>/var/log/aptitude</"
"filename>."

#. type: Content of: <chapter><section><note><para>
#: en/about.dbk:114
msgid ""
"You should take some time to review and remove any sensitive and/or "
"confidential information from the logs before including them in a bug report "
"as the information will be published in a public database."
msgstr ""
"Hauríeu de dedicar un cert temps a revisar i esborrar qualsevol informació "
"sensible i/o confidencial als fitxers de registre abans d'incloure'ls a "
"l'informe d'error, ja que la informació es publicarà a una base de dades "
"pública."

#. type: Content of: <chapter><section><title>
#: en/about.dbk:123
msgid "Sources for this document"
msgstr "Font d'este document"

#. type: Content of: <chapter><section><para>
#: en/about.dbk:125
msgid ""
"The source of this document is in DocBook XML<indexterm><primary>DocBook "
"XML</primary></indexterm> format. The HTML version is generated using "
"<systemitem role=\"package\">docbook-xsl</systemitem> and <systemitem role="
"\"package\">xsltproc</systemitem>. The PDF version is generated using "
"<systemitem role=\"package\">dblatex</systemitem> or <systemitem role="
"\"package\">xmlroff</systemitem>. Sources for the Release Notes are "
"available in the Git repository of the <emphasis>Debian Documentation "
"Project</emphasis>.  You can use the <ulink url=\"&url-vcs-release-notes;"
"\">web interface</ulink> to access its files individually through the web "
"and see their changes.  For more information on how to access Git please "
"consult the <ulink url=\"&url-ddp-vcs-info;\">Debian Documentation Project "
"VCS information pages</ulink>."
msgstr ""
"El codi font d'este document està en format DocBook "
"XML<indexterm><primary>DocBook XML</primary></indexterm>. La versió HTML es "
"genera utilitzant <systemitem role=\"package\">docbook-xsl</systemitem> i "
"<systemitem role=\"package\">xsltproc</systemitem>. La versió en PDF es "
"genera utilitzant <systemitem role=\"package\">dblatex</systemitem> o bé "
"<systemitem role=\"package\">xmlroff</systemitem>. El codi font de les notes "
"de llançament està en el dipòsit Git del <emphasis>Projecte de documentació "
"de Debian</emphasis>. Podeu utilitzar la <ulink url=\"&url-vcs-release-notes;"
"\">interfície web</ulink> per accedir individualment als seus fitxers des de "
"la web i veure els canvis. Per a més informació de com accedir a Git, "
"consulteu les <ulink url=\"&url-ddp-vcs-info;\">pàgines d'informació de VCS "
"del projecte de documentació de Debian</ulink>."
